from locust import HttpUser, task, between
import pandas as pd

class QuickstartUser(HttpUser):
    wait_time = between(0, 1)
    #host = "https://servicesops.dealshare.in"
    #host = "https://stagingservices.dealshare.in"
    @task()
    def getDiscount(self):

        file = "debug_discount.csv"
        df = pd.read_csv(file)
        for index, row in df.iterrows():
            dealDetailRequests = [
                {
                "id":107205,
                "dealId": "new_bng76414",
                "dealPrice": 188,
                "dealType": "main",
                "isFrd": "",
                "quantity": "1",
                "skuId": None
                }
            ]
            headers = {'content-type': 'application/json', 'Accept': 'application/json', 'Authorization':row["authKey"]}
            data = { "businessModel": "b2c", "dealDetailRequests":dealDetailRequests, "source": "all"}
            get_discount = "/discountservice/api/v1/discount/get-discount"
            resp = self.client.post(get_discount, json = data, headers=headers, name="get_discount")
            print(resp)