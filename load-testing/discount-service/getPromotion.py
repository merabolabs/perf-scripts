from locust import HttpUser, task, between
import pandas as pd

class QuickstartUser(HttpUser):
    wait_time = between(0, 1)
    #host = "https://servicesops.dealshare.in"
    #host = "https://stagingservices.dealshare.in"
    @task()
    def getDiscount(self):
        headers = {'content-type': 'application/json', 'Accept': 'application/json'}
        file = "debug_discount.csv"
        df = pd.read_csv(file)
        for index, row in df.iterrows():
            userRank = row['userRank']
            promotionPlanId = row['discountPlanId']
            pinCode = row['pinCode']

            data = { "userRank": userRank, "promotionPlanId": promotionPlanId, "pinCode" : pinCode}
            get_promotion_plan = "/discountservice/api/v1/promotion/get-promotion-plan"
            resp = self.client.post(get_promotion_plan, json = data, headers=headers, name="get_promotion_plan")